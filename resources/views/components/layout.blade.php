<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">

        <title>{{ $title ?? 'Affiliates' }}</title>
        <style>
            .container {
                max-width: 500px;
            }
            dl, ol, ul {
                margin: 0;
                padding: 0;
                list-style: none;
            }
        </style>
    </head>

    <body>
        <div id="app">
            <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
                <div class="container">
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="/">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('affiliates.upload-nearest') }}">Upload Affiliates</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            @if(!empty($slot))
            <div class="container mt-5">
                <h3 class="text-center mb-5">{{ $header ?? 'Affiliates' }}</h3>
                {{ $slot }}
            </div>
            @endif
        </div>
    </body>
</html>
