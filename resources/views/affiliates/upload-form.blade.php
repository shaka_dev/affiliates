<x-layout>
    <x-slot name="title">
        Upload File With Affiliate
    </x-slot>

    <x-slot name="header">
        Upload File With Affiliate
    </x-slot>

    <x-slot name="slot">
        <form action="{{ route('affiliates.show-nearest') }}" method="post" enctype="multipart/form-data">
            @csrf
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <strong>{{ $message }}</strong>
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="custom-file">
                <input type="file" name="file" class="custom-file-input" id="chooseFile">
                <label class="custom-file-label" for="chooseFile">Select file</label>
            </div>

            <button type="submit" name="submit" class="btn btn-primary btn-block mt-4">
                Submit
            </button>
        </form>
    </x-slot>
</x-layout>
