<x-layout>
    <x-slot name="title">
        Upload File With Affiliate
    </x-slot>

    <x-slot name="header">
        Affiliates within 100km list
    </x-slot>

    <x-slot name="slot">
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th>Name</th>
                <th>ID</th>
            </tr>
            </thead>
            <tbody>
            @if(! empty($affiliates))
            @foreach($affiliates as $affiliate)
                <tr>
                    <td>{!! $affiliate->name !!}</td>
                    <td>{!! $affiliate->affiliate_id !!}</td>
                </tr>
            @endforeach
            @endif
            </tbody>
        </table>
    </x-slot>
</x-layout>
