<?php

use App\Http\Controllers\AffiliateController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('affiliates/upload', [AffiliateController::class, 'createForm'])->name('affiliates.upload-nearest');
Route::post('affiliates/show-nearest', [AffiliateController::class, 'showNearest'])->name('affiliates.show-nearest');
