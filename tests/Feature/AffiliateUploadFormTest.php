<?php

namespace Tests\Feature;

use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class AffiliateUploadFormTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->validator = app('validator');
    }

    /**
     * A basic unit test example.
     *
     * @return void
     * @throws \Exception
     */
    public function test_txt()
    {
        $stub = __DIR__.'/../stubs/test.txt';
        $name = bin2hex(random_bytes(10)) . '.txt';
        $path = sys_get_temp_dir().'/'.$name;
        copy($stub, $path);
        $file = new UploadedFile($path, $name, 'text/plain', null, true);
        $response = $this->call('POST', route('affiliates.show-nearest'), [], [], ['file' => $file]);
        $response->assertStatus(200);

        unlink($path);
    }

    public function test_csv()
    {
        $stub = __DIR__.'/../stubs/test.csv';
        $name = bin2hex(random_bytes(10)) . '.csv';
        $path = sys_get_temp_dir().'/'.$name;
        copy($stub, $path);
        $file = new UploadedFile($path, $name, 'ext/csv', null, true);
        $response = $this->call('POST', route('affiliates.show-nearest'), [], [], ['file' => $file]);
        $response->assertStatus(302);

        unlink($path);
    }
}
