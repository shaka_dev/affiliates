<?php

namespace App\Services;

use Illuminate\Support\Facades\Log;
use JsonException;
use stdClass;

class AffiliateService
{
    private const MAX_DISTANCE = 100; // kilometers
    private const DUBLIN_OFFICE_LATITUDE = 53.3340285;
    private const DUBLIN_OFFICE_LONGITUDE = -6.2535495;
    private const REQUIRED_FIELDS = ['affiliate_id', 'name', 'latitude', 'longitude'];

    /**
     * Parse file and filter affiliates within 100km (self::MAX_DISTANCE) distance
     *
     * @return array
     * @throws JsonException
     */
    public function parseFileAndGetNearestAffiliates(): array
    {
        $content = fopen(request()->file->getRealPath(),'r');
        $affiliates = [];

        while(!feof($content)){
            $row = fgets($content);

            if (empty($row)) {
                continue;
            }

            try {
                $aff_data = json_decode($row, false, 512, JSON_THROW_ON_ERROR);
            } catch (\Exception $e) {
                Log::error("Non valid data. Row: `{$row}`");

                continue;
            }

            foreach (self::REQUIRED_FIELDS as $field) {
                if (empty($aff_data->{$field})) {
                    Log::error("Non valid data. The field `{$field}` is required. Row: `{$row}`");

                    continue 2;
                }
            }

            $distance = $this->getDistance(self::DUBLIN_OFFICE_LATITUDE, self::DUBLIN_OFFICE_LONGITUDE, $aff_data->latitude, $aff_data->longitude);

            if ($distance <= self::MAX_DISTANCE) {
                $affiliates[$aff_data->affiliate_id] = $aff_data;
            }
        }

        usort($affiliates, function (stdClass $a, stdClass $b) {
            return ($a->affiliate_id < $b->affiliate_id) ? -1 : 1;
        });

        return $affiliates;
    }

    /**
     * Calculate distance from two points with latitude and longitude
     *
     * @param float $latitudeFrom
     * @param float $longitudeFrom
     * @param float $latitudeTo
     * @param float $longitudeTo
     *
     * @return float [km]
     */
    private function getDistance(float $latitudeFrom, float $longitudeFrom, float $latitudeTo, float $longitudeTo): float
    {
        $rad = M_PI / 180;
        $theta = $longitudeFrom - $longitudeTo;
        $dist = sin($latitudeFrom * $rad) * sin($latitudeTo * $rad)
            +  cos($latitudeFrom * $rad) * cos($latitudeTo * $rad) * cos($theta * $rad);

        return acos($dist) / $rad * 60 *  1.853;
    }
}
