<?php

namespace App\Http\Requests;

use App\Rules\FileMimeTypes;
use Illuminate\Foundation\Http\FormRequest;

class UploadAffiliateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'file' => [
                'required',
                'max:2048',
                new FileMimeTypes('txt'),
            ],
        ];
    }
}
