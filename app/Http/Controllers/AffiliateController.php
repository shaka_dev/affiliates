<?php

namespace App\Http\Controllers;

use App\Http\Requests\UploadAffiliateRequest;
use App\Services\AffiliateService;
use Illuminate\Contracts\View\View;
use JsonException;

class AffiliateController extends Controller
{
    /**
     * @return View
     */
    public function createForm(): View
    {
        return view('affiliates.upload-form');
    }

    /**
     * @param UploadAffiliateRequest $request
     * @param AffiliateService $service
     *
     * @return View
     * @throws JsonException
     */
    public function showNearest(UploadAffiliateRequest $request, AffiliateService $service)
    {
        return view('affiliates.show-nearest', ['affiliates' => $service->parseFileAndGetNearestAffiliates()]);
    }
}
