<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class FileMimeTypes implements Rule
{
    private const MIMETYPES_BY_EXTENSION = [
        'txt' => 'text/plain',
        'csv' => 'ext/csv',
        'pdf' => 'application/pdf',
    ];

    private array $mimeTypes = [];

    /**
     * Create a new rule instance.
     *
     * @param array|string $extension
     *
     * @return void
     */
    public function __construct($extension)
    {
        foreach ((array) $extension as $ext) {
            if (empty(self::MIMETYPES_BY_EXTENSION[$ext])) {
                continue;
            }

            $this->mimeTypes[] = self::MIMETYPES_BY_EXTENSION[$ext];
        }
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $mime = $value->getClientMimeType();

        return in_array($mime, $this->mimeTypes);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return sprintf("Field :attribute must be one of these mime types: %s", implode(', ', $this->mimeTypes));
    }
}
